#include "mem.h"
#include <assert.h>
#include <stdio.h>

#define LINE "-------------------------------------"
#define SMALL_BLOCK_SIZE 100
#define MEDIUM_BLOCK_SIZE 200
#define LARGE_BLOCK_SIZE 100000
#define EXPANDED_BLOCK_SIZE 200000

void start_test(const char *test_name, void (*function)()) {
    heap_init(0);
    printf("%s\n", LINE);
    printf("%s\n", test_name);
    function();
    heap_term();
    printf("Test %s: Success.\n", test_name);
    printf("%s\n", LINE);
}

void assert_condition(bool condition, const char *message) {
    if (!condition) {
        printf("Assertion failed: %s\n", message);
        assert(condition);
    }
}

void test_successful_memory_allocation() {
    void *mem1 = _malloc(SMALL_BLOCK_SIZE);
    assert_condition(mem1 != NULL, "Memory allocation failed.");
    _free(mem1);
}

void test_single_block_free() {
    void *mem1 = _malloc(SMALL_BLOCK_SIZE);
    void *mem2 = _malloc(MEDIUM_BLOCK_SIZE);
    _free(mem1);
    _free(mem2);
}

void test_double_block_free() {
    void *mem1 = _malloc(SMALL_BLOCK_SIZE);
    void *mem2 = _malloc(MEDIUM_BLOCK_SIZE);
    _free(mem1);
    _free(mem2);
}

void test_memory_expansion() {
    void *mem1 = _malloc(LARGE_BLOCK_SIZE);
    assert_condition(mem1 != NULL, "Memory allocation failed.");
    _free(mem1);
}

void test_memory_expansion_with_address_limitation() {
    void *mem1 = _malloc(LARGE_BLOCK_SIZE);
    void *mem2 = _malloc(LARGE_BLOCK_SIZE);
    assert_condition(mem1 != NULL && mem2 != NULL, "Memory allocation failed.");

    _free(mem1);

    void *mem3 = _malloc(EXPANDED_BLOCK_SIZE);
    assert_condition(mem3 != NULL, "Memory expansion with address limitation failed.");

    _free(mem2);
    _free(mem3);
}

int main() {
    start_test("TEST #1: successful_memory_allocation", test_successful_memory_allocation);
    start_test("TEST #2: single_block_free", test_single_block_free);
    start_test("TEST #3: double_block_free", test_double_block_free);
    start_test("TEST #4: memory_expansion", test_memory_expansion);
    start_test("TEST #5: memory_expansion_with_address_limitation", test_memory_expansion_with_address_limitation);
    return 0;
}
